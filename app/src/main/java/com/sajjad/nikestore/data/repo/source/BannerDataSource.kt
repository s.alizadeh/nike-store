package com.sajjad.nikestore.data.repo.source

import com.sajjad.nikestore.data.Banner
import io.reactivex.Single

interface BannerDataSource {
    fun getBanners(): Single<List<Banner>>
}