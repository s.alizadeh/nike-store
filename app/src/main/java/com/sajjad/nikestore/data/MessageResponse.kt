package com.sajjad.nikestore.data

data class MessageResponse(
    val message: String
)