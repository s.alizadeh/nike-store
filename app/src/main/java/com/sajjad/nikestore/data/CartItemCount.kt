package com.sajjad.nikestore.data

data class CartItemCount(
    var count: Int
)