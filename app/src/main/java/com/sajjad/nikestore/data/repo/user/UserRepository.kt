package com.sajjad.nikestore.data.repo.user

import io.reactivex.Completable

interface UserRepository {

    fun signUp(email:String, password:String):Completable

    fun login(email:String, password:String):Completable

    fun loadToken()

    fun getUserName():String

    fun signOut()
}