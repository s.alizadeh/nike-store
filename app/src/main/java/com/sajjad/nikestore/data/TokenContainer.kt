package com.sajjad.nikestore.data

import timber.log.Timber

object TokenContainer {
    var token: String? = null
        private set
    var refreshToken: String? = null
        private set

    fun update(token:String?,refreshToken:String?){
        this.token = token
        this.refreshToken = refreshToken

        Timber.i("Token is -> ${token?.substring(0,10)}, RefreshToken is-> $refreshToken")
    }
}