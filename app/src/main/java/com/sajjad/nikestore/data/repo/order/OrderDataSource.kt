package com.sajjad.nikestore.data.repo.order

import com.sajjad.nikestore.data.Checkout
import com.sajjad.nikestore.data.SubmitOrderResponse
import io.reactivex.Single

interface OrderDataSource {

    fun submit(firstName: String, lastName: String, postalCode: String, phoneNumber: String, address: String, paymentMethod: String)
    :Single<SubmitOrderResponse>

    fun checkOut(orderId:Int):Single<Checkout>
}