package com.sajjad.nikestore.data.repo.source

import com.sajjad.nikestore.data.Comment
import com.sajjad.nikestore.services.http.ApiService
import io.reactivex.Single

class CommentRemoteDataSource(val apiService: ApiService) :CommentDataSource{

    override fun getAll(productId: Int): Single<List<Comment>> = apiService.getComments(productId.toString())

    override fun insert(): Single<Comment> {
        TODO("Not yet implemented")
    }
}