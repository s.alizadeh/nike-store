package com.sajjad.nikestore.feature.profile

import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.TokenContainer
import com.sajjad.nikestore.data.repo.user.UserRepository

class ProfileViewModel(val userRepository: UserRepository) : NikeViewModel() {
    val username: String
        get() = userRepository.getUserName()

    val isSignedIn: Boolean
        get() = TokenContainer.token !=null

    fun signOut() = userRepository.signOut()
}