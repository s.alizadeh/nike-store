package com.sajjad.nikestore.feature.common

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.formatPrice
import com.sajjad.nikestore.common.implementSpringAnimationTrait
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.view.NikeImageView

class PopularProductListAdapter(val imageLoadingService: ImageLoadingService) :
    RecyclerView.Adapter<PopularProductListAdapter.ViewHolder>() {
    // TODO: 6/9/2021
    var onProductClickListener: ProductListAdapter.ProductEventListener? = null

    var popularProducts = ArrayList<Product>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindProduct(popularProducts[position])

    override fun getItemCount(): Int = popularProducts.size

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val productIv: NikeImageView = itemView.findViewById(R.id.product_iv)
        val titleTv: TextView = itemView.findViewById(R.id.title_tv)
        val previousPriceTv: TextView = itemView.findViewById(R.id.product_previous_price_tv)
        val currentPriceTv: TextView = itemView.findViewById(R.id.product_current_price_tv)

        // val favoriteIv: ImageView = itemView.findViewById(R.id.product_favorite_iv)

        fun bindProduct(product: Product) {
            imageLoadingService.load(productIv, product.image)
            titleTv.text = product.title
            previousPriceTv.text = formatPrice(product.previous_price)
            currentPriceTv.text = formatPrice(product.price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener() {
// TODO: 6/9/2021
                onProductClickListener?.onProductClick(product)

            }
        }
    }


    // TODO: 6/9/2021
    interface OnProductClickListener {
        fun onProductClick(product: Product)
    }

}