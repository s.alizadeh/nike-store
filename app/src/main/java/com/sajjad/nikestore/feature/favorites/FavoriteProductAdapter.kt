package com.sajjad.nikestore.feature.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.view.NikeImageView

class FavoriteProductAdapter(
    val products: MutableList<Product>,
    val imageLoadingService: ImageLoadingService,
    val productFavoriteProductEventListener: FavoriteProductEventListener
) : RecyclerView.Adapter<FavoriteProductAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_favotire_product, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(products[position])

    override fun getItemCount(): Int = products.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productImage = itemView.findViewById<NikeImageView>(R.id.favorite_product_iv)
        val productName = itemView.findViewById<TextView>(R.id.favorite_product_tv)

        fun bind(product: Product) {
            imageLoadingService.load(productImage, product.image)
            productName.text = product.title

            itemView.setOnClickListener {
                productFavoriteProductEventListener.onClick(product)
            }

            itemView.setOnLongClickListener {
                productFavoriteProductEventListener.onLongClick(product)
                notifyItemRemoved(adapterPosition)
                products.remove(product)

                return@setOnLongClickListener false
            }
        }
    }


    interface FavoriteProductEventListener {
        fun onClick(product: Product)

        fun onLongClick(product: Product)
    }
}