package com.sajjad.nikestore.feature.profile


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.NikeFragment
import com.sajjad.nikestore.feature.auth.AuthActivity
import com.sajjad.nikestore.feature.favorites.FavoriteProductActivity
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment:NikeFragment() {
    val viewModel:ProfileViewModel by inject()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favorite_btn.setOnClickListener {
            startActivity(Intent(requireContext(),FavoriteProductActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        checkAuthState()
    }

    private fun checkAuthState() {
        if (viewModel.isSignedIn){
            username_tv.text = viewModel.username
            auth_btn.text = getString(R.string.signOut)
            auth_btn.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_sign_out,0)
            auth_btn.setOnClickListener {
                viewModel.signOut()
                checkAuthState()
            }
        }else{
            username_tv.text = getString(R.string.guest_user)
            auth_btn.text = getString(R.string.signUp_screen_title)
            auth_btn.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_sign_in,0)
            auth_btn.setOnClickListener {
                startActivity(Intent(requireContext(),AuthActivity::class.java))
            }
        }
    }
}