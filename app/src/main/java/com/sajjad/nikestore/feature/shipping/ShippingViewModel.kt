package com.sajjad.nikestore.feature.shipping

import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.Checkout
import com.sajjad.nikestore.data.SubmitOrderResponse
import com.sajjad.nikestore.data.repo.order.OrderRepository
import io.reactivex.Single

const val PAYMENT_METHOD_ONLINE = "online"
const val PAYMENT_METHOD_COD = "cash_on_delivery"

class ShippingViewModel(val orderRepository: OrderRepository) : NikeViewModel() {

    fun submitOrder(
        firstName: String,
        lastName: String,
        postalCode: String,
        phoneNumber: String,
        address: String,
        paymentMethod: String
    ): Single<SubmitOrderResponse> {
        return orderRepository.submit(
            firstName,
            lastName,
            postalCode,
            phoneNumber,
            address,
            paymentMethod
        )
    }

    fun checkoutOrder(orderId: Int): Single<Checkout> {
        return orderRepository.checkOut(orderId)
    }
}