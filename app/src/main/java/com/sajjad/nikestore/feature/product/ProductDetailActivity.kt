package com.sajjad.nikestore.feature.product

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.*
import com.sajjad.nikestore.data.Comment
import com.sajjad.nikestore.feature.product.comment.CommentListActivity
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.view.scroll.ObservableScrollViewCallbacks
import com.sajjad.nikestore.view.scroll.ScrollState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_product_detail.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductDetailActivity : NikeActivity() {
    val productDetailViewModel: ProductDetailViewModel by viewModel { parametersOf(intent.extras) }
    val imageLoadingService: ImageLoadingService by inject()
    val commentListAdapter = CommentListAdapter(false)
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        getSharedPreferences("app_settings", MODE_PRIVATE).apply {
            Log.i("TAG", "onCreate: ${this.getString(ACCESS_TOKEN_KEY,"def value")} ")
        }

        toolbar_back_btn.setOnClickListener{
            finish()

        }

        productDetailViewModel.productLiveData.observe(this, {

            imageLoadingService.load(product_iv, it.image)
            title_tv.text = it.title
            current_price_tv.text = formatPrice(it.price)
            previous_price_tv.text = formatPrice(it.previous_price)
            previous_price_tv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            toolbar_title.text = it.title

        })


        productDetailViewModel.commentsLiveData.observe(this) {
            commentListAdapter.comments = it as ArrayList<Comment>
            if (it.size > 3) view_all_comment_btn.visibility = View.VISIBLE

        }

        productDetailViewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        initViews()

    }


    fun initViews() {

        product_iv.post {
            val productIvHeight = product_iv.height
            val toolbar = toolbar_view
            val productIv = product_iv

            observable_scroll_view.addScrollViewCallbacks(object : ObservableScrollViewCallbacks {
                override fun onScrollChanged(
                    scrollY: Int,
                    firstScroll: Boolean,
                    dragging: Boolean
                ) {
                    toolbar.alpha = scrollY.toFloat() / productIvHeight.toFloat()
                    productIv.translationY = scrollY.toFloat() / 2
                }

                override fun onDownMotionEvent() {

                }

                override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) {

                }
            })
        }

        // for comment
        comment_rv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        comment_rv.adapter = commentListAdapter
        comment_rv.isNestedScrollingEnabled = false

        // for view all comments
        view_all_comment_btn.setOnClickListener{
            startActivity(Intent(this,CommentListActivity::class.java).apply {
                putExtra(EXTRA_KEY_ID,productDetailViewModel.productLiveData.value!!.id)
            })
        }

        add_to_cart_btn.setOnClickListener {
            productDetailViewModel.onAddCartBtn()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        showSnackBar(getString(R.string.success_add_to_cart))
                    }

                })
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}