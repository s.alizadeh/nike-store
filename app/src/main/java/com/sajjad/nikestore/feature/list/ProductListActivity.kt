package com.sajjad.nikestore.feature.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.EXTRA_KEY_SORT
import com.sajjad.nikestore.common.NikeActivity
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.feature.common.ProductListAdapter
import com.sajjad.nikestore.feature.common.VIEW_TYPE_LARGE
import com.sajjad.nikestore.feature.common.VIEW_TYPE_SMALL
import com.sajjad.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.activity_product_list.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class ProductListActivity : NikeActivity(),ProductListAdapter.ProductEventListener {
    val productListViewModel: ProductListViewModel by viewModel {
        parametersOf(
            intent.extras!!.getInt(
                EXTRA_KEY_SORT
            )
        )
    }

    val productLisAdapter: ProductListAdapter by inject { parametersOf(VIEW_TYPE_SMALL) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        val gridLayoutManager = GridLayoutManager(this, 2)
        product_list_rv.layoutManager = gridLayoutManager
        product_list_rv.adapter = productLisAdapter

        productListViewModel.productLiveData.observe(this) {
            Timber.i((it.toString()))
            productLisAdapter.products = it as ArrayList<Product>
        }

        productListViewModel.selectedSortTitleLiveData.observe(this) {
            sort_title_tv.text = getString(it)
        }

        sort_changer_btn.setOnClickListener {
            val dialog = MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.sort))
                .setSingleChoiceItems(
                    R.array.sort_title_array,
                    productListViewModel.sort
                ) { dialog, selectedSortIndex ->
                    productListViewModel.changeSortByUser(selectedSortIndex)
                    dialog.dismiss()
                }
            dialog.show()
        }

        view_type_changer_btn.setOnClickListener {
            if (productLisAdapter.viewType == VIEW_TYPE_SMALL) {
                view_type_changer_btn.setImageResource(R.drawable.ic_large)
                productLisAdapter.viewType = VIEW_TYPE_LARGE
                productLisAdapter.notifyDataSetChanged()
                gridLayoutManager.spanCount = 1
            } else {
                view_type_changer_btn.setImageResource(R.drawable.ic_grid)
                productLisAdapter.viewType = VIEW_TYPE_SMALL
                productLisAdapter.notifyDataSetChanged()
                gridLayoutManager.spanCount = 2
            }
        }

        productListViewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        toolbar.onBackClickListener = View.OnClickListener {
            finish()
        }

        productLisAdapter.productEventListener = this

    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(this,ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,product)
        })
    }

    override fun onFavoriteBtnClick(product: Product) {
        productListViewModel.addProductToFavorite(product)
    }


}