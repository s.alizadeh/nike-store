package com.sajjad.nikestore.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.formatPrice
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.data.CartItem
import com.sajjad.nikestore.data.PurchaseDetail
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_purchase_detail.view.*

const val VIEW_TYPE_ITEM_CART = 1
const val VIEW_TYPE_PURCHASE_DETAIL = 2

class CartItemAdapter(
    val imageLoadingService: ImageLoadingService,
    val cartItemViewCallBacks: CartItemViewCallBacks,
    var cartItems: MutableList<CartItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var purchaseDetail: PurchaseDetail? = null

    inner class CartItemViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindCartItem(cartItem: CartItem) {
            containerView.product_title_tv.text = cartItem.product.title
            containerView.previous_price_tv.text = formatPrice(cartItem.product.previous_price)
            containerView.price_tv.text = formatPrice(cartItem.product.price)
            containerView.cart_item_count_tv.text = cartItem.count.toString()
            imageLoadingService.load(containerView.product_iv, cartItem.product.image)

            containerView.remove_from_cart_btn.setOnClickListener {
                cartItemViewCallBacks.onRemoveCartItemBtnClick(cartItem)
            }

            containerView.decrease_iv.setOnClickListener {
                if (cartItem.count > 1) {
                    cartItem.changeCountProgressBarIsVisible = true
                    containerView.change_count_progress_bar.visibility = View.VISIBLE
                    containerView.cart_item_count_tv.visibility = View.INVISIBLE
                    cartItemViewCallBacks.onDecreaseCartItemBtnClick(cartItem)
                }
            }

            containerView.increase_iv.setOnClickListener {
                if (cartItem.count<5){
                    cartItem.changeCountProgressBarIsVisible = true
                    containerView.change_count_progress_bar.visibility = View.VISIBLE
                    containerView.cart_item_count_tv.visibility = View.INVISIBLE
                    cartItemViewCallBacks.onIncreaseCartItemBtnClick(cartItem)
                }



            }




            containerView.product_iv.setOnClickListener {
                cartItemViewCallBacks.onProductImageClick(cartItem)
            }

            containerView.change_count_progress_bar.visibility =
                if (cartItem.changeCountProgressBarIsVisible) View.VISIBLE else View.GONE

            containerView.cart_item_count_tv.visibility =
                if (cartItem.changeCountProgressBarIsVisible) View.INVISIBLE else View.VISIBLE

        }
    }

    class PurchaseDetailViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindPurchaseDetail(totalPrice: Int, shippingCost: Int, payablePrice: Int) {
            containerView.total_price_tv.text = formatPrice(totalPrice)
            containerView.shipping_cost_tv.text = formatPrice(shippingCost)
            containerView.payable_price_tv.text = formatPrice(payablePrice)
        }
    }

    interface CartItemViewCallBacks {
        fun onProductImageClick(cartItem: CartItem)
        fun onIncreaseCartItemBtnClick(cartItem: CartItem)
        fun onDecreaseCartItemBtnClick(cartItem: CartItem)
        fun onRemoveCartItemBtnClick(cartItem: CartItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM_CART) {
            return CartItemViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
            )
        } else
            return PurchaseDetailViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_purchase_detail, parent, false)
            )

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            holder.bindCartItem(cartItems[position])
        } else if (holder is PurchaseDetailViewHolder) {
            purchaseDetail?.let {
                holder.bindPurchaseDetail(it.total_price, it.shipping_cost, it.payable_price)
            }
        }
    }

    override fun getItemCount(): Int = cartItems.size + 1

    override fun getItemViewType(position: Int): Int {
        if (position == cartItems.size)
            return VIEW_TYPE_PURCHASE_DETAIL
        else
            return VIEW_TYPE_ITEM_CART
    }

    fun removeItem(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun changeCount(cartItem: CartItem) {
        val index = cartItems.indexOf(cartItem)
        if (index > -1) {
            cartItems[index].changeCountProgressBarIsVisible = false
            notifyItemChanged(index)


        }
    }
}