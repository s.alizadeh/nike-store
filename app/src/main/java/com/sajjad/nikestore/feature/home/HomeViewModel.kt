package com.sajjad.nikestore.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.common.NikeCompletableObserver
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.Banner
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.SORT_LATEST
import com.sajjad.nikestore.data.SORT_POPULAR
import com.sajjad.nikestore.data.repo.banner.BannerRepository
import com.sajjad.nikestore.data.repo.product.ProductRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(
    private val productRepository: ProductRepository,
    bannerRepository: BannerRepository
) :
    NikeViewModel() {

    private val _latestProductLiveData = MutableLiveData<List<Product>>()
    val latestProductLiveData: LiveData<List<Product>>
        get() = _latestProductLiveData


    private val _bannerLiveData = MutableLiveData<List<Banner>>()
    val bannerLiveData: LiveData<List<Banner>>
        get() = _bannerLiveData

    private val _bannerHeightLiveData = MutableLiveData<Int>()
    val bannerHeightLiveData: LiveData<Int>
    get() = _bannerHeightLiveData


    private val _popularProductLiveData = MutableLiveData<List<Product>>()
    val popularProductLiveData: LiveData<List<Product>>
        get() = _popularProductLiveData

    init {
        progressLiveData.value = true

        productRepository.getProducts(SORT_LATEST)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                progressLiveData.value = false
            }
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(t: List<Product>) {
                    _latestProductLiveData.value = t
                }


            })

        //for banner
        bannerRepository.getBanners()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeSingleObserver<List<Banner>>(compositeDisposable) {
                override fun onSuccess(t: List<Banner>) {
                    _bannerLiveData.value = t
                }

            })


        productRepository.getProducts(SORT_POPULAR)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                progressLiveData.value = false
            }
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(t: List<Product>) {
                    _popularProductLiveData.value = t
                }

            })


    }


    fun saveHeight(height: Int) {
        _bannerHeightLiveData.value = height
    }

    fun addProductToFavorite(product: Product) {
        if (product.isFavorite) {
            productRepository.deleteFromFavorites(product)
                .subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        product.isFavorite = false
                    }
                })
        } else {
            productRepository.addToFavorite(product)
                .subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        product.isFavorite = true
                    }
                })
        }
    }
}