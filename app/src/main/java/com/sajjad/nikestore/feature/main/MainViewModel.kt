package com.sajjad.nikestore.feature.main

import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.repo.cart.CartRepository
import com.sajjad.nikestore.data.CartItemCount
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus

class MainViewModel(private val cartRepository: CartRepository):NikeViewModel() {
    fun getItemCount(){
        cartRepository.getCartCount()
            .subscribeOn(Schedulers.io())
            .subscribe(object :NikeSingleObserver<CartItemCount>(compositeDisposable){
                override fun onSuccess(t: CartItemCount) {
                    EventBus.getDefault().postSticky(t)
                }

            })
    }
}