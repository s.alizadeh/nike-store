package com.sajjad.nikestore.feature.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.EXTRA_KEY_SORT
import com.sajjad.nikestore.common.NikeFragment
import com.sajjad.nikestore.common.convertDpToPixel
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.SORT_LATEST
import com.sajjad.nikestore.data.SORT_POPULAR
import com.sajjad.nikestore.feature.main.BannerSliderAdapter
import com.sajjad.nikestore.feature.common.PopularProductListAdapter
import com.sajjad.nikestore.feature.common.ProductListAdapter
import com.sajjad.nikestore.feature.common.VIEW_TYPE_ROUND
import com.sajjad.nikestore.feature.list.ProductListActivity
import com.sajjad.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class HomeFragment : NikeFragment(), ProductListAdapter.ProductEventListener {

    val homeViewModel: HomeViewModel by viewModel()
    val productListAdapter: ProductListAdapter by inject { parametersOf(VIEW_TYPE_ROUND) }
    val popularProductListAdapter: PopularProductListAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productListAdapter.productEventListener = this
        // TODO: 6/9/2021
        popularProductListAdapter.onProductClickListener = this

        latest_product_rv.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        latest_product_rv.adapter = productListAdapter

        popular_product_rv.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        popular_product_rv.adapter = popularProductListAdapter


        //for latest product
        homeViewModel.latestProductLiveData.observe(viewLifecycleOwner) {
            Timber.i(it.toString())
            productListAdapter.products = it as ArrayList<Product>
        }


        // for popular product
        homeViewModel.popularProductLiveData.observe(viewLifecycleOwner) {
            Timber.i(it.toString())
            popularProductListAdapter.popularProducts = it as ArrayList<Product>
        }


        //progress
        homeViewModel.progressLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }

        // for banner
        homeViewModel.bannerLiveData.observe(viewLifecycleOwner) {
            Timber.i(it.toString())
            val bannerSliderAdapter = BannerSliderAdapter(this, it)
            banner_slider_view_pager.adapter = bannerSliderAdapter

            // not used
            val viewPagerHeight = (((banner_slider_view_pager.measuredWidth - convertDpToPixel(
                32f,
                requireContext()
            )) * 173) / 328).toInt()

            val layoutParams = banner_slider_view_pager.layoutParams
            layoutParams.height = 523

            slider_indicator.setViewPager2(banner_slider_view_pager)

            banner_slider_view_pager.layoutParams = layoutParams


        }


        view_latest_btn.setOnClickListener {
            startActivity(Intent(requireContext(), ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_SORT, SORT_LATEST)
            })
        }

        view_popular_btn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_SORT, SORT_POPULAR)
            })
        }

    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(requireContext(), ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, product)
        })
    }

    override fun onFavoriteBtnClick(product: Product) {
        homeViewModel.addProductToFavorite(product)
    }
}