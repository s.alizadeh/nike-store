package com.sajjad.nikestore.feature.cart

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.NikeCompletableObserver
import com.sajjad.nikestore.common.NikeFragment
import com.sajjad.nikestore.feature.auth.AuthActivity
import com.sajjad.nikestore.feature.product.ProductDetailActivity
import com.sajjad.nikestore.feature.shipping.ShippingActivity
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.data.CartItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cart.*
import kotlinx.android.synthetic.main.view_cart_empty_state.*
import kotlinx.android.synthetic.main.view_cart_empty_state.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CartFragment : NikeFragment(), CartItemAdapter.CartItemViewCallBacks {
    val viewModel: CartViewModel by viewModel()
    var cartItemAdapter: CartItemAdapter? = null
    val imageLoadingService: ImageLoadingService by inject()
    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.progressLiveData.observe(viewLifecycleOwner) {
            setProgressIndicator(it)
        }

        viewModel.cartItemsLiveData.observe(viewLifecycleOwner) {
            Timber.i(it.toString())
            cart_items_rv.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            cartItemAdapter =
                CartItemAdapter(imageLoadingService, this, it as MutableList<CartItem>)
            cart_items_rv.adapter = cartItemAdapter

        }

        viewModel.purchaseDetailLiveData.observe(viewLifecycleOwner) {
            Timber.i(it.toString())
            cartItemAdapter?.let { adapter ->
                adapter.purchaseDetail = it
                adapter.notifyItemChanged(adapter.cartItems.size)
            }

        }

        viewModel.emptyStateLiveData.observe(viewLifecycleOwner){
            val emptyState = showEmptyState(R.layout.view_cart_empty_state)
            emptyState?.let { view->
                if (it.mustShow){
                    view.empty_state_message_tv.text = getString(it.messageResId)
                    view.empty_state_cta_btn.visibility = if (it.mustShowCallToActionButton) View.VISIBLE else View.GONE
                    view.empty_state_cta_btn.setOnClickListener {
                        startActivity(Intent(requireContext(),AuthActivity::class.java))
                    }
                }
                else empty_state_root_view?.visibility = View.GONE

            }
        }

        pay_btn.setOnClickListener {
            startActivity(Intent(requireContext(),ShippingActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA,viewModel.purchaseDetailLiveData.value)
            })
        }

    }

    override fun onStart() {
        super.onStart()
        viewModel.refresh()
    }

    override fun onProductImageClick(cartItem: CartItem) {
        startActivity(Intent(requireContext(), ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, cartItem.product)
        })
    }

    override fun onIncreaseCartItemBtnClick(cartItem: CartItem) {
        viewModel.increaseCartItemCount(cartItem)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.changeCount(cartItem)
                }
            })
    }

    override fun onDecreaseCartItemBtnClick(cartItem: CartItem) {
        viewModel.decreaseCartItemCount(cartItem)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.changeCount(cartItem)
                }
            })
    }

    override fun onRemoveCartItemBtnClick(cartItem: CartItem) {
        viewModel.removeFromCart(cartItem)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                override fun onComplete() {
                    cartItemAdapter?.removeItem(cartItem)
                }

            })
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}