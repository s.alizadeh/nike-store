package com.sajjad.nikestore.services.http

import com.google.gson.JsonObject
import com.sajjad.nikestore.data.TokenContainer
import com.sajjad.nikestore.data.repo.source.CLIENT_ID
import com.sajjad.nikestore.data.repo.source.CLIENT_SECRET
import com.sajjad.nikestore.data.repo.source.UserDataSource
import com.sajjad.nikestore.data.TokenResponse
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber
import java.lang.Exception

class NikeAuthenticator : Authenticator, KoinComponent {

    val apiService: ApiService by inject()
    val userLocalDataSource: UserDataSource by inject()

    override fun authenticate(route: Route?, response: Response): Request? {

        try {
            if (TokenContainer.token != null && TokenContainer.refreshToken != null && !response.request
                            .url.pathSegments.last().equals("token",false)
            ) {


                val token = refreshToken()
                if (token.isEmpty())
                    return null

                return response.request.newBuilder()
                    .addHeader("Authorization", "Bearer ${TokenContainer.token}").build()
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            Timber.i("Token is null")
        }

        return null
    }

    fun refreshToken(): String {
        val response: retrofit2.Response<TokenResponse> =
            apiService.refreshToken(JsonObject().apply {
                addProperty("grant_type", "refresh_token")
                addProperty("refresh_token", TokenContainer.refreshToken)
                addProperty("client_id", CLIENT_ID)
                addProperty("client_secret", CLIENT_SECRET)
            }).execute()
        response.body()?.let {
            TokenContainer.update(it.access_token, it.refresh_token)
            userLocalDataSource.saveToken(it.access_token, it.refresh_token)
            return it.access_token
        }
        return ""
    }
}