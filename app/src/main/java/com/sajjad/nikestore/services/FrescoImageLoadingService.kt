package com.sajjad.nikestore.services

import com.facebook.drawee.view.SimpleDraweeView
import com.sajjad.nikestore.view.NikeImageView
import java.lang.IllegalStateException

class FrescoImageLoadingService : ImageLoadingService {
    override fun load(imageView: NikeImageView, imageUrl: String) {
        if (imageView is SimpleDraweeView)
            imageView.setImageURI(imageUrl)
        else
            throw IllegalStateException("image view must be instance of SimpleDraweeView")
    }
}