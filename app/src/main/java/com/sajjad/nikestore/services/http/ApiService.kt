package com.sajjad.nikestore.services.http

import com.google.gson.JsonObject
import com.sajjad.nikestore.data.*
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {
    @GET("product/list")
    fun getProducts(@Query("sort") sort: String): Single<List<Product>>

    @GET("banner/slider")
    fun getBanners(): Single<List<Banner>>

    @GET("comment/list")
    fun getComments(@Query("product_id") productId: String): Single<List<Comment>>


    // cart
    @POST("cart/add")
    fun addToCart(@Body jsonObject: JsonObject): Single<AddToCartResponse>

    @POST("cart/remove")
    fun removeFromCart(@Body jsonObject: JsonObject): Single<MessageResponse>

    @POST("cart/changeCount")
    fun changeCartCount(@Body jsonObject: JsonObject): Single<AddToCartResponse>

    @GET("cart/list")
    fun getCartItems(): Single<CartResponse>

    @GET("cart/count")
    fun getCartCount(): Single<CartItemCount>

    @POST("order/submit")
    fun submitOrder(@Body jsonObject: JsonObject): Single<SubmitOrderResponse>

    @GET("order/checkout")
    fun checkOut(@Query("order_id") orderId: Int): Single<Checkout>

    @POST("auth/token")
    fun login(@Body jsonObject: JsonObject): Single<TokenResponse>

    @POST("user/register")
    fun signUp(@Body jsonObject: JsonObject): Single<MessageResponse>

    @POST("authToken")
    fun refreshToken(@Body jsonObject: JsonObject): Call<TokenResponse>
}

fun createApiServiceInstance(): ApiService {

    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor {
            val oldRequest = it.request()

            val newRequest = oldRequest.newBuilder()
            if (TokenContainer.token != null)
                newRequest.addHeader("Authorization", "Bearer ${TokenContainer.token}")
            newRequest.addHeader("accept", "application/json")

            newRequest.method(oldRequest.method, oldRequest.body)
            return@addInterceptor it.proceed(newRequest.build())

        }
        .addInterceptor(HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        })
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl("http://expertdevelopers.ir/api/v1/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()



    return retrofit.create(ApiService::class.java)


}