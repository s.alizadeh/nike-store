package com.sajjad.nikestore

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import androidx.room.Room
import com.facebook.drawee.backends.pipeline.Fresco
import com.sajjad.nikestore.data.db.AppDatabase
import com.sajjad.nikestore.data.repo.*
import com.sajjad.nikestore.data.repo.banner.BannerRepository
import com.sajjad.nikestore.data.repo.banner.BannerRepositoryImpl
import com.sajjad.nikestore.data.repo.cart.CartRepository
import com.sajjad.nikestore.data.repo.cart.CartRepositoryImpl
import com.sajjad.nikestore.data.repo.comment.CommentRepository
import com.sajjad.nikestore.data.repo.comment.CommentRepositoryImpl
import com.sajjad.nikestore.data.repo.order.OrderRemoteDataSource
import com.sajjad.nikestore.data.repo.order.OrderRepository
import com.sajjad.nikestore.data.repo.order.OrderRepositoryImpl
import com.sajjad.nikestore.data.repo.product.ProductRepository
import com.sajjad.nikestore.data.repo.product.ProductRepositoryImpl
import com.sajjad.nikestore.data.repo.source.*
import com.sajjad.nikestore.data.repo.user.UserRepository
import com.sajjad.nikestore.data.repo.user.UserRepositoryImpl
import com.sajjad.nikestore.feature.auth.AuthViewModel
import com.sajjad.nikestore.feature.cart.CartViewModel
import com.sajjad.nikestore.feature.checkout.CheckoutViewModel
import com.sajjad.nikestore.feature.home.HomeViewModel
import com.sajjad.nikestore.feature.common.PopularProductListAdapter
import com.sajjad.nikestore.feature.common.ProductListAdapter
import com.sajjad.nikestore.feature.favorites.FavoriteProductAdapter
import com.sajjad.nikestore.feature.favorites.FavoriteProductViewModel
import com.sajjad.nikestore.feature.list.ProductListViewModel
import com.sajjad.nikestore.feature.main.MainViewModel
import com.sajjad.nikestore.feature.product.ProductDetailViewModel
import com.sajjad.nikestore.feature.product.comment.CommentListViewModel
import com.sajjad.nikestore.feature.profile.ProfileViewModel
import com.sajjad.nikestore.feature.shipping.ShippingViewModel
import com.sajjad.nikestore.services.FrescoImageLoadingService
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.services.http.createApiServiceInstance
import io.reactivex.Single
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
        Fresco.initialize(this)

        val myModules = module {

            single { createApiServiceInstance() }

            single<ImageLoadingService> { FrescoImageLoadingService() }

            single { Room.databaseBuilder(this@App,AppDatabase::class.java, "db_app").build() }

            factory<ProductRepository> {
                ProductRepositoryImpl(
                    ProductRemoteDataSource(get()),
                    get<AppDatabase>().getDao()
                )
            }

            single<SharedPreferences> {
                this@App.getSharedPreferences(
                    "app_settings",
                    MODE_PRIVATE
                )
            }

            single { UserLocalDataSource(get()) }

            factory { (viewType: Int) -> ProductListAdapter(viewType, get()) }
            factory { PopularProductListAdapter(get()) }

            factory<BannerRepository> { BannerRepositoryImpl(BannerRemoteDataSource(get())) }

            factory<CommentRepository> { CommentRepositoryImpl(CommentRemoteDataSource(get())) }

            factory<CartRepository> { CartRepositoryImpl(CartRemoteDataSource(get())) }

            single<OrderRepository> { OrderRepositoryImpl(OrderRemoteDataSource(get())) }

            single<UserRepository> {
                UserRepositoryImpl(
                    UserRemoteDataSource(get()),
                    UserLocalDataSource(get())
                )
            }

            viewModel { HomeViewModel(get(), get()) }
            viewModel { (bundle: Bundle) -> ProductDetailViewModel(bundle, get(), get()) }
            viewModel { (productId: Int) -> CommentListViewModel(get(), productId) }
            viewModel { (sort: Int) -> ProductListViewModel(sort, get()) }
            viewModel { AuthViewModel(get()) }
            viewModel { CartViewModel(get()) }
            viewModel { MainViewModel(get()) }
            viewModel { ShippingViewModel(get()) }
            viewModel { (orderId: Int) -> CheckoutViewModel(orderId, get()) }
            //
            viewModel { ProfileViewModel(get()) }
            viewModel { FavoriteProductViewModel(get()) }
        }

        startKoin {
            androidContext(this@App)
            modules(myModules)
        }

        val userRepository: UserRepository = get()
        userRepository.loadToken()
    }
}