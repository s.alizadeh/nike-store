package com.sajjad.nikestore.common

const val EXTRA_KEY_DATA = "data"
const val EXTRA_KEY_ID = "id"
const val EXTRA_KEY_SORT = "id"

const val ACCESS_TOKEN_KEY = "access_token"
const val REFRESH_TOKEN_KEY = "refresh_token"
