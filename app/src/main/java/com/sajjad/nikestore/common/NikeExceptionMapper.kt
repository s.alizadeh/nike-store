package com.sajjad.nikestore.common

import com.sajjad.nikestore.R
import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber
import java.lang.Exception

class NikeExceptionMapper {

    companion object {

        fun map(throwable: Throwable): NikeException {

            try {
                if (throwable is HttpException) {

                    val jsonObjectError = JSONObject(throwable.response()?.errorBody()!!.string())
                    val serverErrorMessage = jsonObjectError.getString("message")
                    return NikeException(
                        if (throwable.code() == 401) NikeException.Type.AUTH else NikeException.Type.SIMPLE,
                        serverMessage = serverErrorMessage
                    )

                }
            } catch (exception: Exception) {
                Timber.e(exception)
            }

            return NikeException(NikeException.Type.SIMPLE, R.string.unknown_error)


        }

    }
}